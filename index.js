const express = require('express');
const nunjucks = require('nunjucks');

const app = express();

nunjucks.configure('views', {
  autoescape: true,
  express: app,
  watch: true,
});

app.use(
  express.urlencoded({
    extended: true,
  })
);

const checkAge = (req, res, next) => {
  const { age } = req.query;
  if (!age) {
    console.log('Não passou o query param age');
    return res.redirect('/');
  }
  return next();
};

app.set('view engine', 'njk');

app.get('/', (req, res) => {
  return res.render('home');
});

app.post('/check', (req, res) => {
  const { age } = req.body;
  if (age < 18) {
    return res.redirect(`/minor?age=${age}`);
  } else {
    return res.redirect(`/major?age=${age}`);
  }
});

app.get('/major', checkAge, (req, res) => {
  const { age } = req.query;
  return res.render('check', {
    message: `Você é maior de idade e possui ${age} anos!`,
  });
});

app.get('/minor', checkAge, (req, res) => {
  const { age } = req.query;
  return res.render('check', {
    message: `Você é menor de idade, só possui ${age} anos!`,
  });
});

app.listen(process.env.PORT || 3000);
